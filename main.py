#!/usr/local/bin/python
# -*- coding: utf-8 -*-

__author__ = 'pavel'
from lxml import html
import requests
import codecs
from bs4 import BeautifulSoup
import re


baseUrl = "http://dns-shop.ru"
cookie = {'city_path': 'rostov-na-donu'}

urlPath = "catalog/ucenennye_tovary"
getParams = "length_1={itemsPerPage}&price_item_multigroup[]={deviceGroup}"


class Item(object):
    def __init__(self, id, url, title, specialization, reason, shops, currentPrice, oldPrice, mark):
        self.id = int(id)
        self.url = url
        self.title = title
        self.specialization = specialization
        self.reason = reason
        self.shops = shops
        self.currentPrice = currentPrice
        self.oldPrice = oldPrice
        self.mark = mark

    def __str__(self):
        shopstr= u""
        for x in self.shops:
            shopstr += u"" + x
        return u'''id = {id}
title = {title}
url = {url}
specialization = {specialization}
reason = {reason}
shops = [{shops}]'''.format(id=self.id
                            , title=self.title
                            , url=self.url
                            , specialization=self.specialization
                            , reason=self.reason
                            , shops=shopstr)



def get_html_content(deviceID = 10, itemsPerPage = 0):
    params = getParams.format(itemsPerPage=itemsPerPage, deviceGroup=deviceID)
    url = "%s/%s/?%s" % (baseUrl, urlPath, params)
    print url
    r = requests.get(url, cookies=cookie)
    if r.status_code != 200:
        print "Not ok {url}".format(url=url)
        return ""
    print r.text
    return r.text


def get_file_content():
    with codecs.open('/home/pavel/code/python/dns_shop_parcer/pages/p1.html', 'r', 'utf-8') as f:
        content = f.read()
        # print content
        return content


def getcontent(deviceID = 10, itemsPerPage = 0):
    return get_file_content()
    # return get_html_content(deviceID,itemsPerPage)


def get_spec_items(spec_str, pattern="\,(?=\ )|\ *"):
    find_spec_items(spec_str)
    # spec_str = spec_str.replace("/", ", ")
    # res = re.split(pattern, spec_str)
    # resstr = ""
    # for x in res:
    #     if x is None or len(x) < 1:
    #         continue
    #     resstr += "[" + x + "]"
    # print "=*= " + resstr + " =*="


def find_spec_items(text):
    # r"[а-я\w]+[\.\,/\-]{0,1}\w*"
    res = set(re.findall(ur"[а-яА-Я\w]+[\.,/\-]?(?!\s|$)[а-яА-Я\w]*",text))
    resstr = ""
    for x in res:
        if x is None or len(x) < 1:
            continue
        resstr += "[" + x + "]"
    print "=*= " + resstr + " =*="


def get_available_device(dom):
    for x in dom.xpath("//select[@name='price_item_multigroup']/option"):
        id = x.xpath(".//@value")[0].lstrip().rstrip()
        title = x.text.lstrip().rstrip()
        yield (id, title)


def get_all(deviceID = 10, itemsPerPage = 0):
    soup = BeautifulSoup(getcontent(deviceID, itemsPerPage))
    parcered_content = html.document_fromstring(soup.prettify("utf-8"))
    xpathQuery = "//table[contains(@class,'catalog_view_list')]/tbody/tr[not(contains(@class,'section'))]"
    trList = parcered_content.xpath(xpathQuery)
    print len(trList)

    for tr in xrange(0, len(trList), 2):
        id = trList[tr].xpath(".//td[@class='code']/text()")[0].lstrip().rstrip()
        titleNode = trList[tr].xpath(".//td[@class ='title']")[0]
        a = titleNode.xpath(".//a[not(@class='image')]")[0]
        title = a.text.lstrip().rstrip()
        href = a.xpath(".//@href")[0].lstrip().rstrip()
        try:
            specStr = titleNode.xpath(".//p[@class='spec']/text()")[0].lstrip().rstrip()
        except IndexError:
            specStr = ""

        # get_spec_items(title + " " + specStr)
        try:
            reasonStr = titleNode.xpath(".//p[@class='reason']/text()")[0].lstrip().rstrip()
            # get_spec_items(reasonStr, "\,\ ")
        except IndexError:
            reasonStr  = "No reason"

        try:
            markStr = trList[tr].xpath(".//td[@class ='opinion']/a/@title")[0].lstrip().rstrip()
            # print markStr
        except IndexError:
            markStr = ""
            pass

        price = trList[tr].xpath(".//td[@class ='price']")[0]
        try:
            current = price.xpath(".//div[@class='new']/text()")[0].lstrip().rstrip()
            old_prise = price.xpath(".//div[@class='prev']/s/text()")[0].lstrip().rstrip()
        except IndexError:
            current = price.xpath(".//text()")[0].lstrip().rstrip()
            old_prise = None

        availList = []
        try:
            for x in trList[tr].xpath(".//td[@class='shop_avail']/a/@title"):
                availList.append(x.lstrip().rstrip())
            for x in trList[tr +1].xpath(".//td[@class='shop_avail']/a/@title"):
                availList.append(x.lstrip().rstrip())

        except IndexError:
            print "Not available"
        item = Item(id, href, title, specStr, reasonStr, availList, current, old_prise, markStr)
        yield item

for item in get_all():
    print item.__str__()
    print "===="
    break
